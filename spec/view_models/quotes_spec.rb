# frozen_string_literal: true
require "./view_models/quotes"
RSpec.describe ViewModels::Quotes do
  subject { described_class.new(params) }
  let(:params) do
    {
      author: "Arno Fleming",
      date:   Date.iso8601("2015-01-02"),
      quote:  "I spy, I spy\n\nWith my little eye",
    }
  end

  it "leaves the author as is " do
    expect(subject.author)
      .to eq("Arno Fleming")
  end

  it "converts newlines to <br> tags in the quote" do
    expect(subject.quote)
      .to eq("I spy, I spy<br>\n<br>\nWith my little eye")
  end

  it "parses the date as a string" do
    expect(subject.date)
      .to eq("2 Jan 2015")
  end
end
