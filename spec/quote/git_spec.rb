# frozen_string_literal: true
require "./lib/quote/git"

RSpec.describe Quote::Git do
  let(:location) do
    # TODO: inject the location dependency: No need to clone repos in test
    "https://github.com/kabisa/maji.git"
  end

  describe "#new" do
    it "must be initialized with a location" do
      expect { described_class.new }
        .to raise_error(ArgumentError, "missing keyword: location")
    end

    it "can be initialized with a https repo location" do
      expect { described_class.new(location: location) }.not_to raise_error
    end
  end

  describe "#random_quote" do
    subject { described_class.new(location: location).random_quote }

    it { is_expected.to have_key(:quote) }
    it { is_expected.to have_key(:author) }
    it { is_expected.to have_key(:date) }
  end
end
