# frozen_string_literal: true
module ViewModels
  class Quotes
    attr_reader :author

    def initialize(params = {})
      @quote =  params.fetch :quote
      @author = params.fetch :author
      @date =   params.fetch :date
    end

    def quote
      @quote.gsub("\n", "<br>\n")
    end

    def date
      @date.strftime("%-d %b %Y")
    end
  end
end
