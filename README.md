# QuoteEm - a git quotes database

Use this app to see (possibly funny) quotes

## How to set up
clone this repo, run `./entrypoint.sh` and you're all set.  We're supposed to
live in docker, so for best experience in development, just go to the parent directory,
run  `docker-compose build` and then `docker-compose run web`.

Then navigate to
* http://localhost:9292/ to get started

### Run your tests

`docker-compose run web ./entrypoint.sh guard` - will run the magnificent guard
`docker-compose run web ./entrypoint.sh rspec` - will just run the specs

Well... everything you put after `./entrypoint.sh` will be passed on to
`bundle exec`, so you can run all kind of niceties in there :D
