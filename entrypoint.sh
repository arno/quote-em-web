#!/bin/sh
SUCCES=0
NOT_BUNDLED=41

bundle check || bundle install

if [ $? -ne $SUCCES ] ; then
  echo "your bundle is outdated, I cannot install it. please rebuild!"
  exit $NOT_BUNDLED
fi

if [ $# -ne 0 ]; then
  exec bundle exec $@
else
  current_ip=$(ip addr show dev eth0 | grep "inet " | cut -d ' ' -f 6  | cut -f 1 -d '/')
  echo "I'm starting a server on $current_ip"
  exec bundle exec rackup --host 0.0.0.0
fi
