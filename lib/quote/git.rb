# frozen_string_literal: true
module Quote
  class Git
    def initialize(location:)
      @location = location
      @name = repo_name

      generate_repo_log
    end

    def random_quote
      commit_quotes.sample
    end

    private

    attr_reader :name, :repo_log

    def commit_quotes
      @commit_quotes ||= non_merge_commits.map do |commit|
        {
          author: commit.author.name,
          quote:  commit.message,
          date:   commit.date,
        }
      end
    end

    def non_merge_commits
      @non_merge_commits ||=
        repo_log.reject { |commit| commit.parents.count == 2 }
    end

    def generate_repo_log
      return if defined?(@repo_log)

      clone_repo
      parse_log
    end

    def already_cloned?
      File.directory?(repo_path)
    end

    def clone_repo
      ::Git.clone(@location, repo_name, path: repos_path) unless already_cloned?
    end

    def parse_log
      @repo_log = ::Git::Log.new(::Git.open(repo_path))
    end

    def repo_name
      @name ||=
        @location
        .split("/")
        .last
        .sub(".git", "")
    end

    def repos_path
      Pathname.new "/tmp/"
    end

    def repo_path
      @repo_path ||= File.join(repos_path, repo_name)
    end
  end
end
