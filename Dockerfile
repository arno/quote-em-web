FROM ruby:2.3.3-alpine
MAINTAINER Arno Fleming

# TODO: Check if I realy need to set UTF8 on Alpine
ENV LANG=C.UTF-8

WORKDIR /home/app

COPY ./Gemfile /home/app
COPY ./Gemfile.lock /home/app

RUN  apk update \
  # dependencies for the project
  && apk add git \
  # dependencies for development
  && apk add less \
  # dependencies for building (note: these apks will be removed after the `bundle install` step!
  && apk add --virtual build_deps make gcc libc-dev libffi \
  && bundle install \
  && apk del build_deps \
  && adduser -D -h /home/app -g '' quotes

EXPOSE 9292

CMD ["./entrypoint.sh"]

USER quotes

COPY . /home/app
