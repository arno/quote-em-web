# frozen_string_literal: true
require "sinatra"
require "git"
require "./lib/quote"

class GitQuote < Sinatra::Base
  require "./view_models/quotes"

  get "/" do
    @title = "Git Quote of the Moment!"
    git_quote = ::Quote::Git.new(location: "https://github.com/kabisa/maji.git").random_quote
    quote_locals = ViewModels::Quotes.new(git_quote)

    erb :quotes, {}, locals: {
      quote:  quote_locals.quote,
      author: quote_locals.author,
      date:   quote_locals.date,
    }
  end
end
